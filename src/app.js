/* 
Custom Implementierung des bpmn-js Framework im Rahmen einer Projektarbeit an der Hochschule Rhein Main
© Steven Nagy, Miguel Kreiter, Jannik Burggraf
29.08.2020
*/

//Import der benötigten Module
//const BpmnJS = require('bpmn-js/lib/Modeler');
import BpmnJS from 'bpmn-js/lib/Modeler';
//const CliModule = require('bpmn-js-cli');
import CliModule from 'bpmn-js-cli';


//Initiales BPMN laden (Bietet unsere Grundstruktur)
var diagramUrl = './initial.bpmn';

// Modeler Instanz mit CLI Module laden
var bpmnModeler = new BpmnJS({
  container: '#canvas',
  additionalModules: [
    CliModule
  ],
  cli: {
    bindTo: 'cli'
  },
  keyboard: {
    bindTo: window
  }
});


/**
 * Open diagram in our modeler instance.
 *
 * @param {String} bpmnXML diagram to display
 */
function openDiagram(bpmnXML) {

  // import diagram
  bpmnModeler.importXML(bpmnXML, function (err) {

    if (err) {
      return console.error('could not import BPMN 2.0 diagram', err);
    }

    // access modeler components
    var canvas = bpmnModeler.get('canvas');
    var overlays = bpmnModeler.get('overlays');
    var elementFactory = bpmnModeler.get("elementFactory");

    // zoom to fit full viewport
    canvas.zoom('fit-viewport');


  });
}

// load external diagram file via AJAX and open it
$.get(diagramUrl, openDiagram, 'text');


// Submit Function. Beginn der Custom Logik
$("#bpmnForm").submit(function (e) {

  fetch('http://localhost:3000/getbpmn')
  .then(response => response.json())
  .then(data => console.log(data));

  // Verhindert den Submit Default (u.a. den Page Reload)
  e.preventDefault();

  // Prüfe auf fehlerhafte Felder in der Form (Nutze Bootstrap Implementiertung)
  (function () {
    'use strict';
    window.addEventListener('load', function () {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();


  //Wir nutzen hier ein Zusammenspiel aus ElementFactory und der Erweiterung CLI.
  var canvas = bpmnModeler.get('canvas');
  var overlays = bpmnModeler.get('overlays');
  var elementFactory = bpmnModeler.get("elementFactory");
  var cli = window.cli;
  var root = canvas.getRootElement();

  var startEvent_1 = elementFactory.createBpmnElement("shape", {
    type: "bpmn:StartEvent",
    x: 100,
    y: 120
  });
  canvas.addShape(startEvent_1, root);

  //Schreibe alle Form Elemente in ein Array
  const forms = document.querySelectorAll('form');
  const form = forms[0];

  //Hilfsvariablen zum befüllen
  var descriptionvalue = "";
  var selectvalue = "";

  //Output Array:
  var finalOutput = [];

  //Finales Array befüllen
  [...form.elements].forEach((input) => {

    //Nicht benötigte Felder herausfiltern
    if (input.name != "add" && input.name != "remove" && input.name != "submit") {
      //console.log(input);

      //Textbeschreibung
      if (input.name.includes("text")) {
        descriptionvalue = input.value;
      };

      //Elementtyp
      if (input.id.includes("select")) {
        selectvalue = input.value;

        //Objekt Array besteht aus einer Beschreibung, Elementtyp, Knotenbeschriftung, technischer ID und Lane (aktuell nicht genutzt)
        finalOutput.push({ description: descriptionvalue, elementType: selectvalue, nodeName: input.name, techName: "", lane: "0" });

        //2er Paar fertig, wieder initial setzen
        descriptionvalue = "";
        selectvalue = "";
      };

    };
  });


  //console.log(finalOutput);
  //Über finales Array iterieren um Objekte für den Canvas zu erzeugen. Append erstellt automatisch die Sequenzflüsse
  var gatewayopenPos = 0;
  var gatewayopenID;
  var gatewayclosedPos = 0;
  var gatewayclosedID;
  var tempskip = 0;
  for (let i = 0; i < finalOutput.length; i++) {

    //Erstes Element an StartEvent appenden
    if (i == 0) {

      //console.log("Erstelle mit " + finalOutput[i].elementType + " ein Element");

      var task = cli.append(
        startEvent_1,
        'bpmn:' + finalOutput[i].elementType + '',
        '150,0'
      );
      cli.setLabel(task, finalOutput[i].description);
      finalOutput[i].techName = task;

      //Gateway gefunden ODER Gateway offen
    } else if (finalOutput[i].elementType.includes("Gateway") || gatewayopenPos != 0) {

      //Gateway öffnet. Weitere Logik muss innerhalb dieses Blocks erfolgen
      if (gatewayopenPos == 0) {
        gatewayopenPos = i; //Gateway an Stelle i
        gatewayclosedPos = 0; // Gateway schließen init

        //console.log("Erstelle mit " + finalOutput[i].elementType + " ein öffnendes Gateway");

        //Gateway erstellen
        var gateway = cli.append(
          task,
          'bpmn:' + finalOutput[i].elementType + '',
          '150,0'
        );
        gatewayopenID = gateway;
        finalOutput[i].techName = gateway;

        //Schließendes Gateway kommt
      } else if (finalOutput[i].elementType.includes("Gateway") && gatewayopenPos != 0) {
        gatewayclosedPos = i; //Gateway an Stelle i
        gatewayopenPos = 0; // Gateway öffnen init - Verlässt diese Schleife im nächsten Durchlauf

        //console.log("Erstelle mit " + finalOutput[i].elementType + " ein schließendes Gateway");

        //Gateway erstellen
        var gateway = cli.append(
          task1,
          'bpmn:' + finalOutput[i].elementType + '',
          '150,0'
        );
        gatewayclosedID = gateway;
        finalOutput[i].techName = gateway;

        cli.connect(
          task2,
          gateway,
          'bpmn:SequenceFlow'
        );


        // Normale Elemente innerhalb des Gateways
      } else {

        //console.log("Erstelle mit " + finalOutput[i].elementType + " ein Element");

        //Erste Elemente der Lane können mit Gateway verbunden werden, weitere an die tasks in der lane
        if (tempskip == 0) {

          var task1 = cli.append(
            gateway,
            'bpmn:' + finalOutput[i].elementType + '',
            '150,0'
          );

          cli.setLabel(task1, finalOutput[i].description);
          finalOutput[i].techName = task1;

          var task2 = cli.append(
            gateway,
            'bpmn:' + finalOutput[i + 1].elementType + '',
            '150,100'
          );

          cli.setLabel(task2, finalOutput[i + 1].description);
          finalOutput[i + 1].techName = task2;

          i++;

          tempskip = 1;

          //Weitere Elemmente in der Lane, mit vorherigenm Task in Lane verbinden
        } else {

          var task1 = cli.append(
            task1,
            'bpmn:' + finalOutput[i].elementType + '',
            '150,0'
          );

          cli.setLabel(task1, finalOutput[i].description);
          finalOutput[i].techName = task1;

          var task2 = cli.append(
            task2,
            'bpmn:' + finalOutput[i + 1].elementType + '',
            '150,0'
          );

          cli.setLabel(task2, finalOutput[i + 1].description);
          finalOutput[i + 1].techName = task2;

          i++;

        }
      }


      //kein Gateway erkannt und keine offenes Gateway vorhanden -> normales Element 
    } else if (!finalOutput[i].elementType.includes("Gateway") && gatewayopenPos == 0) {

      //console.log("Erstelle mit " + finalOutput[i].elementType + " ein Element");

      //Wieder init setzen
      tempskip = 0;

      //Wenn vorheriges Element das schließende Gateway war -> an Gateway appenden
      if (finalOutput[i - 1].elementType.includes("Gateway")) {

        var task = cli.append(
          gateway,
          'bpmn:' + finalOutput[i].elementType + '',
          '150,0'
        );

        cli.setLabel(task, finalOutput[i].description);
        finalOutput[i].techName = task;

        //normales Element erzeugen
      } else {

        var task = cli.append(
          task,
          'bpmn:' + finalOutput[i].elementType + '',
          '150,0'
        );

        cli.setLabel(task, finalOutput[i].description);
        finalOutput[i].techName = task;
      }

    }

    //Schleife Ende - Array und Elemente vollständig erzeugt
  }

  //console.log(finalOutput);

  //EndEvent erstellen
  cli.append(
    task,
    'bpmn:EndEvent',
    '150,0'
  );


  //Nötig da Singe Page Application. Verstecke FormContainer und zeige Canvas zum Rendern des BPMN. Button um neu zu starten
  document.getElementById('formContainer').style.display = 'none';
  document.getElementById('canvas').style.opacity = '1';
  document.getElementById('save-button').style.display = 'block';
});

//Lade Page neu um ein neues BPMN zu modellieren
window.ShowControlBar = function () {
  //document.getElementById('formContainer').style.display = 'block';
  //document.getElementById('canvas').style.opacity = '0';
  //document.getElementById('save-button').style.display = 'none';
  //$.get(diagramUrl, openDiagram, 'text');

  location.reload();
};





//Erzeugung des Formulars mit Buttons
$(document).ready(function () {
  //Hilfsvariablen (i=Element in Form, j=Child Element in Form. z.B. kann Element 3 ein Root Element sein und n Child Elemente besitzen)
  var i = 1;
  var j = 0;

  //Add Function (Neues Element)
  $('#add').click(function () {
    i++;
    $('#dynamic_field').append(`
           <tr id="row${i}">
           <td>
           <input type="text" id="text${i}" name="text${i}" placeholder="Bezeichnung" class="form-control" required /><div class="invalid-feedback">Bitte Bezeichnung eingeben.</div>
           </td>
           <td>
           <select class="form-control" id="select${i}" name="${i}">
           <optgroup label="Aufgaben">
           <option value="Task">Aufgabe</option>
           <option value="SendTask">Sender</option>
           <option value="ReceiveTask">Empfänger</option>
           <option value="UserTask">Benutzeraufgabe</option>
           <option value="ManualTask">Manuelle Aufgabe</option>
           <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
           <option value="ScriptTask">Script Aufgabe</option>
           <option value="ServiceTask">Service Aufgabe</option>
           </optgroup>
           <optgroup id="group${i}" label="Gateways">
        <option value="ExclusiveGateway">Exklusive (XOR) Vernküpfung</option>
        <option value="ParallelGateway">Parallele (AND) Verknüpfung</option>
        <option value="InclusiveGateway">Inklusive (Kombination aus XOR/AND) Vernküpfung</option>
        <option value="EventBasedGateway">Event-basierte Verknüpfung</option>
        <option value="ComplexGateway">Komplexe Verknüpfung</option>
        </optgroup>
           </select>
           </td>
           <td>
           <button type="button" name="remove" id="${i}" class="btn btn-danger btn_remove">X</button>
           </td>
           </tr>
           `);

    //Entferne letzten Remove Button und Gateways aus der Auswahl, nur für das letzte Element möglich.
    if (i >= 3) {
      var counter_group = i - 1;
      document.getElementById(i - 1).style.display = 'none';
      document.getElementById("group" + counter_group).style.display = 'none';
    }

    //Change Event Listener für die Select Boxen (Prüfe auf Gateways)
    let sel = document.getElementById('select' + i);
    sel.addEventListener("change", function () {
      if (this.value.includes("Gateway")) {
        j++;
        $('#dynamic_field').append(`
              <tr id="row${i}_${j}">
              <td id="column${i}_${j}_1">
              <input type="text" name="text${i}_${j}_1" placeholder="Bezeichnung" class="form-control" required /><div class="invalid-feedback">Bitte Bezeichnung eingeben.</div>
              <select style="margin-top: 10px;" class="form-control" id="select${i}_${j}_1" name="${i}_${j}_1">
              <optgroup label="Aufgaben">
              <option value="Task">Aufgabe</option>
              <option value="SendTask">Sender</option>
              <option value="ReceiveTask">Empfänger</option>
              <option value="UserTask">Benutzeraufgabe</option>
              <option value="ManualTask">Manuelle Aufgabe</option>
              <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
              <option value="ScriptTask">Script Aufgabe</option>
              <option value="ServiceTask">Service Aufgabe</option>
              </optgroup>
              </select>
              </td>
              <td id="column${i}_${j}_2">
              <input type="text" name="text${i}_${j}_2" placeholder="Bezeichnung" class="form-control" required /><div class="invalid-feedback">Bitte Bezeichnung eingeben.</div>
              <select style="margin-top: 10px;" class="form-control" id="select${i}_${j}_2" name="${i}_${j}_2">
              <optgroup label="Aufgaben">
              <option value="Task">Aufgabe</option>
              <option value="SendTask">Sender</option>
              <option value="ReceiveTask">Empfänger</option>
              <option value="UserTask">Benutzeraufgabe</option>
              <option value="ManualTask">Manuelle Aufgabe</option>
              <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
              <option value="ScriptTask">Script Aufgabe</option>
              <option value="ServiceTask">Service Aufgabe</option>
              </optgroup>
              </select>
              </td>
              <td>
              <button type="button" name="remove" id="${i}_${j}" class="btn btn-danger btn_remove_child">X</button><br>
              <button style="margin-top: 10px;" type="button" name="add" id="add_child${i}_${j}" class="btn btn-success btn_add_child">Neues Element in dieser Verzweigung</button><br>
              <button style="margin-top: 10px;" type="button" name="add" id="finish${i}_${j}" class="btn btn-dark btn_finish">Verzweigung abschließen</button>
              </td>
              </tr>
              `);

        //Letzte Buttons entfernen und farbliche Hervorhebung setzen
        document.getElementById(i).style.display = 'none';
        document.getElementById('text' + i).value = '';
        document.getElementById('text' + i).disabled = true;
        document.getElementById('select' + i).disabled = true;
        document.getElementById('row' + i).style.backgroundColor = 'lightblue';
        document.getElementById('row' + i + '_' + j).style.backgroundColor = 'lightblue';
      }
    });
  });


  //Remove Function (X für reguläre Elemente)
  $(document).on('click', '.btn_remove', function () {
    var button_id = $(this).attr("id");
    $('#row' + button_id + '').remove();
    i--;

    if (i >= 2) {
      document.getElementById(i).style.display = 'block';
      document.getElementById("group" + i).style.display = 'block';
    }

  });


  //Remove Function (X für Child Elemente von Verzweigungen)
  $(document).on('click', '.btn_remove_child', function () {

    //Für Root
    if (j == 1) {
      var button_id = $(this).attr("id");
      $('#row' + button_id + '').remove();
      document.getElementById(i).style.display = 'block';
      document.getElementById('row' + i).style.backgroundColor = 'white';
      document.getElementById('text' + i).disabled = false;
      document.getElementById('select' + i).disabled = false;
      document.getElementById('select' + i).value = 'Task';

      //Für Child Elements
    } else {
      var button_id = $(this).attr("id");
      var counter_lastButtons2 = j - 1;
      $('#row' + button_id + '').remove();
      document.getElementById(i + '_' + counter_lastButtons2).style.display = 'block';
      document.getElementById('add_child' + i + '_' + counter_lastButtons2).style.display = 'block';
      document.getElementById('finish' + i + '_' + counter_lastButtons2).style.display = 'block';
    }
    //Ein Element weniger
    j--;

  });

  //Add Function (Neue Elemente für Child Elemente von Verzweigungen)
  $(document).on('click', '.btn_add_child', function () {
    var button_id = $(this).attr("id");
    j++;
    $('#dynamic_field').append(`
              <tr id="row${i}_${j}">
              <td id="column${i}_${j}_1">
              <input type="text" name="text${i}_${j}_1" placeholder="Bezeichnung" class="form-control" required /><div class="invalid-feedback">Bitte Bezeichnung eingeben.</div>
              <select style="margin-top: 10px;" class="form-control" id="select${i}_${j}_1" name="${i}_${j}_1">
              <optgroup label="Aufgaben">
              <option value="Task">Aufgabe</option>
              <option value="SendTask">Sender</option>
              <option value="ReceiveTask">Empfänger</option>
              <option value="UserTask">Benutzeraufgabe</option>
              <option value="ManualTask">Manuelle Aufgabe</option>
              <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
              <option value="ScriptTask">Script Aufgabe</option>
              <option value="ServiceTask">Service Aufgabe</option>
              </optgroup>
              </select>
              <br>
              <button type="button" name="remove" id="column${i}_${j}_1" class="btn btn-danger btn_remove_column">Aufgabe entfernen</button><br>
              </td>
              <td id="column${i}_${j}_2">
              <input type="text" name="text${i}_${j}_2" placeholder="Bezeichnung" class="form-control" required /><div class="invalid-feedback">Bitte Bezeichnung eingeben.</div>
              <select style="margin-top: 10px;" class="form-control" id="select${i}_${j}_2" name="${i}_${j}_2">
              <optgroup label="Aufgaben">
              <option value="Task">Aufgabe</option>
              <option value="SendTask">Sender</option>
              <option value="ReceiveTask">Empfänger</option>
              <option value="UserTask">Benutzeraufgabe</option>
              <option value="ManualTask">Manuelle Aufgabe</option>
              <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
              <option value="ScriptTask">Script Aufgabe</option>
              <option value="ServiceTask">Service Aufgabe</option>
              </optgroup>
              </select>
              <br>
              <button type="button" name="remove" id="column${i}_${j}_2" class="btn btn-danger btn_remove_column">Aufgabe entfernen</button><br>
              </td>
              <td>
              <button type="button" name="remove" id="${i}_${j}" class="btn btn-danger btn_remove_child">X</button><br>
              <button style="margin-top: 10px;" type="button" name="add" id="add_child${i}_${j}" class="btn btn-success btn_add_child">Neues Element in dieser Verzweigung</button><br>
              <button style="margin-top: 10px;" type="button" name="add" id="finish${i}_${j}" class="btn btn-dark btn_finish">Verzweigung abschließen</button>
              </td>
              </tr>
              `);


    var counter_lastButtons = j - 1;
    //Letzte Buttons entfernen und farbliche Hervorhebung setzen
    document.getElementById(i + '_' + counter_lastButtons).style.display = 'none';
    document.getElementById('add_child' + i + '_' + counter_lastButtons).style.display = 'none';
    document.getElementById('finish' + i + '_' + counter_lastButtons).style.display = 'none';
    document.getElementById('row' + i + '_' + j).style.backgroundColor = 'lightblue';


  });

  //Remove Function für Elemente in Gateway
  $(document).on('click', '.btn_remove_column', function () {
    var button_id = $(this).attr("id");

    var testme = confirm("Diese Aufgabe für die Verzweigung wirklich entfernen?");

    if (testme == true) {
      $('#' + button_id + '').text('');
    }


  });

  //Gateway abschließen
  $(document).on('click', '.btn_finish', function () {
    var button_id = $(this).attr("id");

    var selectedValueOfGateway = document.getElementById("select" + i).value;

    i++;
    $('#dynamic_field').append(`
           <tr id="row${i}">
           <td>
           <input type="text" id="text${i}" name="text${i}" placeholder="Bezeichnung" class="form-control" disabled />
           </td>
           <td>
           <select class="form-control" id="select${- 1}_end" name="${i - 1}_end" disabled>
           <optgroup label="Aufgaben">
           <option value="Task">Aufgabe</option>
           <option value="SendTask">Sender</option>
           <option value="ReceiveTask">Empfänger</option>
           <option value="UserTask">Benutzeraufgabe</option>
           <option value="ManualTask">Manuelle Aufgabe</option>
           <option value="BusinessRuleTask">Geschäftsregel Aufgabe</option>
           <option value="ScriptTask">Script Aufgabe</option>
           <option value="ServiceTask">Service Aufgabe</option>
           <option selected value="${selectedValueOfGateway}">Gateway Ende</option>
 
           </select>
           </td>
           <td>
           </td>
           </tr>
           `);



    var counter_lastButtons_i = i - 1;
    //Letzte Buttons entfernen und farbliche Hervorhebung setzen
    document.getElementById(counter_lastButtons_i + '_' + j).style.display = 'none';
    document.getElementById('add_child' + counter_lastButtons_i + '_' + j).style.display = 'none';
    document.getElementById('finish' + counter_lastButtons_i + '_' + j).style.display = 'none';
    document.getElementById('row' + i).style.backgroundColor = 'lightblue';




  });

});


