/* 
Custom Implementierung des bpmn-js Framework im Rahmen einer Projektarbeit an der Hochschule Rhein Main
© Steven Nagy, Miguel Kreiter, Jannik Burggraf
11.03.2021
*/

//Import der benötigten Module
//const BpmnJS = require('bpmn-js/lib/Modeler');
import BpmnJS from 'bpmn-js/lib/Modeler';
//const CliModule = require('bpmn-js-cli');
import CliModule from 'bpmn-js-cli';


//Initiales BPMN laden (Bietet unsere Grundstruktur)
var diagramUrl = './initial.xml';

// Modeler Instanz mit CLI Module laden
var bpmnModeler = new BpmnJS({
  container: '#canvas',
  additionalModules: [
    CliModule
  ],
  cli: {
    bindTo: 'cli'
  },
  keyboard: {
    bindTo: window
  }
});


/**
 * Open diagram in our modeler instance.
 *
 * @param {String} bpmnXML diagram to display
 */
function openDiagram(bpmnXML) {

  // import diagram
  bpmnModeler.importXML(bpmnXML, function (err) {

    if (err) {
      return console.error('could not import BPMN 2.0 diagram', err);
    }

    // access modeler components
    var canvas = bpmnModeler.get('canvas');
    var overlays = bpmnModeler.get('overlays');
    var elementFactory = bpmnModeler.get("elementFactory");

    // zoom to fit full viewport
    canvas.zoom('fit-viewport');


  });
}

// load external diagram file via AJAX and open it
$.get(diagramUrl, openDiagram, 'text');

//Array for technical IDS
let technical_ids = [];
let technical_ids_elements1 = [];
let technical_ids_elements2 = [];
let technical_ids_elements3 = [];
let pool_connector = [];

//Variable for y coordinate
var yaxis = 150;
var poolcounter = 0;
var elementcounter = 0;
var e_skip = false;
var lastitem;
var x = 150;
var y = 150;
var gatewayopen = false;
var connector;
var temp;

//Variablendekleration bpmn-js/cli
const canvas = bpmnModeler.get('canvas');
const overlays = bpmnModeler.get('overlays');
const elementFactory = bpmnModeler.get("elementFactory");
const cli = window.cli;
const root = canvas.getRootElement(); 
const elementRegistry = bpmnModeler.get("elementRegistry");
const modeling = bpmnModeler.get("modeling");


//Entry Point
window.onload = function () {
    console.log('Dokument geladen');

fetch('/getbpmn')
  .then(response => response.json())
  .then(data => BPMNCreator(data));

};

//Mache etwas mit den Daten aus dem GET
function BPMNCreator(data){
  data.pools.forEach(PoolCreator);

  // loop the outer array
  for (let i = 0; i < pool_connector.length; i++) {
    if (pool_connector[i][1] == 1){
      cli.connect(
        pool_connector[i][0],
        technical_ids_elements1[pool_connector[i][2]-1],
        'bpmn:MessageFlow'
      );
    } else if (pool_connector[i][1] == 2){
      cli.connect(
        pool_connector[i][0],
        technical_ids_elements2[pool_connector[i][2]-1],
        'bpmn:MessageFlow'
      );
    } else if (pool_connector[i][1] == 3){
      cli.connect(
        pool_connector[i][0],
        technical_ids_elements3[pool_connector[i][2]-1],
        'bpmn:MessageFlow'
      );
    };
  };
};

//Übergeordneter Pool der Elemente
function PoolCreator(item, index){
  //console.log('Erstelle BPMN Pool ');
  elementcounter = 0; //Reset für jeden Pool

  //Default erstellt keinen Pool
  if (item.description != 'Default'){
    if (index == 0){
      //Prozess bekommen und Pool darauf erstellen
      var process = elementRegistry.get("Process_0sckl64");
      var startEvent = elementRegistry.get("StartEvent_1");

      //Ersten Pool erstellen
      const participant2 = elementFactory.createParticipantShape({
        type: "bpmn:Participant"
      });

      var participant = modeling.createShape(
        participant2,
        { x: 400, y: yaxis },
        process
      );
      cli.setLabel(participant, item.description);
      technical_ids.push(participant);
      yaxis = yaxis + 300;
    } else { 
      
      //Weitere Pools erstellen
      const participant2 = elementFactory.createParticipantShape({
        type: "bpmn:Participant"
      });

  
      //Pool erstellen unterhalb des ersten Pools
      var participant = modeling.createShape(
        participant2,
        { x: 400, y: technical_ids[poolcounter - 1].y + technical_ids[poolcounter - 1].height + 150 },
        technical_ids[0].parent
      );
      cli.setLabel(participant, item.description);
      technical_ids.push(participant); 
      yaxis = yaxis + 300;
    };

  }

    //Elemente innerhalb des Pools
    item.elements.forEach(item => ElementCreator(item));
    poolcounter = poolcounter + 1;
    elementcounter = 0;
    
    console.log(pool_connector);
    e_skip = false;

};

//Einzelnes BPMN Element
function ElementCreator(item){
  //console.log('Erstelle BPMN Element');

  //Stuktur um letztes Element zu bekommen
  if (elementcounter == 0 && poolcounter == 0){
    var lastElement = 'StartEvent_1';
  } else if (poolcounter == 0) {
    var lastElement = cli.element(technical_ids_elements1[elementcounter-1]);
  } else if (poolcounter == 1) {
    var lastElement = technical_ids_elements2[elementcounter-1];
  } else if (poolcounter == 2) {
    var lastElement = technical_ids_elements3[elementcounter-1];
  };


//Wenn zweiter Pool erzeugt wird, diesen als Parent für das erste Element nehmen
if (poolcounter >= 1 && e_skip == false && technical_ids.length >= 1){
  lastElement = technical_ids[poolcounter].id;
  e_skip = true;

  temp = cli.create(
    'bpmn:'+item.element+'',
    { x: technical_ids[poolcounter].x + 100, y: technical_ids[poolcounter].y + 100 },
    lastElement
  );

//Default Logik zur Erzeugung von Elementen
} else {

  //Sämtliche Logik muss in folgenden Block umziehen
  if (item.element.includes('Gateway') && gatewayopen == false){ //Öffnendes Gateway
    console.log('Gateway öffnet!');
    gatewayopen = true;
  } else if (item.element.includes('Gateway') && gatewayopen == true){ //Schließendes Gateway
    console.log('Gateway schließt!');
    gatewayopen = false;
  } else if (!item.element.includes('Gateway') && gatewayopen == true){ //Elemente innerhalb zweier Gateways
    if (item.connects_to){
    } else {
      console.log('Keine Lane angegeben. Setze Element auf die 1. Ebene');
    };
  } else { //Element außerhalb eines Gateways. Normale Behandlung
      //console.log(item);
  };

  
    //Zuweisung von Elementen. Nutzung nach Gateways. Wenn Pool gefüllt ist, ist keine sinnvolle Verwendung möglich.
   if (item.connects_to && !item.connects_to_pool){

    item.connects_to.forEach(function (element, index){
      if (poolcounter == 0 && index == 0){
        connector = cli.element(technical_ids_elements1[element-1]);
      } else if (poolcounter == 1 && index == 0){
        connector = cli.element(technical_ids_elements2[element-1]);
      } else if (poolcounter == 2 && index == 0){
        connector = cli.element(technical_ids_elements3[element-1]);
      };
    });

  
  if (lastitem == connector){
     temp = cli.append(
      connector,
      'bpmn:'+item.element+'',
      '150,'+y+''
    );  
    y = y + 150;
  } else {
    lastitem = connector;    
    temp = cli.append(
      connector,
      'bpmn:'+item.element+'',
      '150,0'
    ); 
    y = 150;
  };

      item.connects_to.forEach(function (element, index){
  
      if (index >= 1 && !item.connects_to_pool){
        if (poolcounter == 0){
          cli.connect(
            cli.element(technical_ids_elements1[element-1]),
            temp,
            'bpmn:SequenceFlow'
          );      
        } else if (poolcounter == 1){
          cli.connect(
            cli.element(technical_ids_elements2[element-1]),
            temp,
            'bpmn:SequenceFlow'
          );    
        } else if (poolcounter == 2){
          cli.connect(
            cli.element(technical_ids_elements3[element-1]),
            temp,
            'bpmn:SequenceFlow'
          );    
        };
      };
    });
    
}//Wenn kein Connects_to gefüllt ist
  else { //Default Element
    temp = cli.append(
      lastElement,
      'bpmn:'+item.element+'',
      '150,0'
    ); 
  };

  if (item.connects_to && item.connects_to_pool){
    //Messageflows
    if (item.connects_to_pool){  
         pool_connector.push([temp,item.connects_to_pool,item.connects_to[0]]); 
     };
  };

  
  //Wenn Beschreibung mitgegeben -> befüllen
  if (item.description){
    cli.setLabel(temp, item.description);
  }
  
};

//Für alle Elemente. Erzeugtes Element dem Array hinzufügen
if (poolcounter == 0){
  technical_ids_elements1.push(temp); 
} else if (poolcounter == 1){
  technical_ids_elements2.push(temp); 
} else if (poolcounter == 2){
  technical_ids_elements3.push(temp); 
};

elementcounter = elementcounter + 1;

};






/* EXTRA FUNKTIONEN */
//Speichern als SVG
window.saveAsSVG = function () {

  bpmnModeler.saveSVG({ format: true }, function (error, svg) {
    if (error) {
      alert('Nicht speicherbar! Kein gültiges BPMN!');
        return;
    }

    var svgBlob = new Blob([svg], {
        type: 'image/svg+xml'
    });

    var fileName = 'BPMN_' + Math.random(36).toString().substring(10) + '.svg';

    var downloadLink = document.createElement('a');
    downloadLink.download = fileName;
    downloadLink.innerHTML = 'Get BPMN SVG';
    downloadLink.href = window.URL.createObjectURL(svgBlob);
    downloadLink.onclick = function (event) {
        document.body.removeChild(event.target);
    };
    downloadLink.style.visibility = 'hidden';
    document.body.appendChild(downloadLink);
    downloadLink.click();                                        
});             
};

//Speichern als XML
window.saveAsXML = function () {
  
  bpmnModeler.saveXML({ format: true }, function (error, xml) {
    if (error) {
        alert('Nicht speicherbar! Kein gültiges BPMN!');
        return;
    }

    var svgBlob = new Blob([xml], {
        type: 'application/xml'
    });

    var fileName = 'BPMN_' + Math.random(36).toString().substring(10) + '.xml';

    var downloadLink = document.createElement('a');
    downloadLink.download = fileName;
    downloadLink.innerHTML = 'Get BPMN XML';
    downloadLink.href = window.URL.createObjectURL(svgBlob);
    downloadLink.onclick = function (event) {
        document.body.removeChild(event.target);
    };
    downloadLink.style.visibility = 'hidden';
    document.body.appendChild(downloadLink);
    downloadLink.click();                                        
});   
};

//Reset Canvas
window.resetCanvas = function () {
  $.get(diagramUrl, openDiagram, 'text');
};

//XML Upload/File Dialog Handling
function handleFileSelect(e) {
  var files = e.target.files;
  if (files.length < 1) {
    alert('Bitte Datei auswählen.');
    return;
  }
  var file = files[0];
  var reader = new FileReader();
  reader.onload = onFileLoaded;
  reader.readAsDataURL(file);
}

function onFileLoaded(e) {
  var match = /^data:(.*);base64,(.*)$/.exec(e.target.result);
  if (match == null) {
    throw 'Could not parse result'; // should not happen
  }
  var mimeType = match[1];
  var content = match[2];

  //Nur .datei und xml erlauben
  if(mimeType=="application/octet-stream" || mimeType=="text/xml" ){

    function b64DecodeUnicode(str) {
      return decodeURIComponent(Array.prototype.map.call(atob(str), function(c) {
          return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
      }).join(''))
  }
  
    var xmlcontent = b64DecodeUnicode(content);
    openDiagram(xmlcontent);

  } else {
    alert("Der mimeType "+mimeType+" wird nicht unterstützt! Bitte XML/BPMN verwenden. ")
  };

  

}

//Event Handler
$(function() {
  $('#import-pfx-button').click(function(e) {
    $('#file-input').click();
  });
  $('#file-input').change(handleFileSelect);
});



//Random ID generieren (Für Dateiname - nicht genutzt)
function createid(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}



