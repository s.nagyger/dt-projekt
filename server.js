// express und http Module importieren. Sie sind dazu da, die HTML-Dateien
// aus dem Ordner "public" zu veröffentlichen.

//const logic = require('./src/app');
const express = require('express');
const router = express.Router();
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
//const blablab = require('./src/app')




const app = express();
const server = require('http').createServer(app);
let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}

// Mit diesem Kommando starten wir den Webserver.
server.listen(port, function () {
    // Wir geben einen Hinweis aus, dass der Webserer läuft.
    console.log('Webserver läuft und hört auf Port %d', port);
});

// Hier teilen wir express mit, dass die öffentlichen HTML-Dateien
// im Ordner "public" zu finden sind.
app.use(express.static(__dirname + '/dist'));
app.use('/scripts', express.static(__dirname + '/node_modules/jsonform/'));

// Stored BPMNs
let BPMNObject;

//Cross-Origin Resource Sharing erlauben. Cross-Origin Requests sind damit möglich
app.use(cors());

// Configuring body parser middleware - Only accept JSON Objects
app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json());

//POST Request
app.post('/createbpmn', (req, res) => {
    
    //Einfache Prüfung
    if (req.body.pools){ //Valides JSON Objekt (Pools vorhanden)
      const newbpmn = req.body;
      BPMNObject = newbpmn;

      res.setHeader('Content-Type', 'text/plain')
      res.write('BPMN hinzugefügt aus Datensatz:\n')
      res.end(JSON.stringify(req.body, null, 2))
 
    }else{ //Kein Valides JSON Objekt (Pools Objekt nicht vorhanden)
      res.setHeader('Content-Type', 'text/plain')
      res.statusCode = 400
      res.write('Keinen Pool im JSON Objekt erkannt. Bitte überprüfen Sie Ihr JSON File:\n')
      res.end(JSON.stringify(req.body, null, 2));
    };
});

//GET Request
app.get('/getbpmn', (req, res) => {

    if (BPMNObject) {
      res.json(BPMNObject);
    } else {
      res.setHeader('Content-Type', 'text/plain')
      res.statusCode = 400
      res.write('Kein BPMN vorhanden. Bitte zuerst per POST Request an /getbpmn erzeugen.')
      res.end();
    };
});



router.get('/creator',function(req,res){
    res.sendFile(path.join(__dirname+'/dist/bpmn_creator.html'));
  });


  router.get('/modeler',function(req,res){
    res.sendFile(path.join(__dirname+'/dist/bpmn_modeler.html'));
  });
  
  //add the router
  app.use('/', router);


  //Mögliche Erweiterung mit einer Datenbank
/*   const MongoClient = require('mongodb').MongoClient;
  

  async function listDatabases(client){
    databasesList = await client.db().admin().listDatabases();
 
    console.log("Databases:");
    databasesList.databases.forEach(db => console.log(` - ${db.name}`));
};

  async function main(){
    /**
     * Connection URI. Update <username>, <password>, and <your-cluster-url> to reflect your cluster.
     * See https://docs.mongodb.com/ecosystem/drivers/node/ for more details
     
    const uri = "mongodb+srv://dbUser:Qwertzu89@dtprojekt.zqa9f.mongodb.net/DTProjekt?retryWrites=true&w=majority";
 

    const client = new MongoClient(uri);
 
    try {
        // Connect to the MongoDB cluster
        await client.connect();
 
        // Make the appropriate DB calls
        await  listDatabases(client);
 
    } catch (e) {
        console.error(e);
    } finally {
        await client.close();
    }
}

main().catch(console.error); */
