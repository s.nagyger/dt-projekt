Master Projekt im Studienfach Wirtschaftsinformatik an der Hochschule RheinMain.

[[_TOC_]]

## Projekt lokal ausführen

**Webserver starten** node server (http://localhost:8000/)

**Javascript bundeln (required!)** npm run build um das Webpack Script zu starten. (Entry Point ist derzeit lediglich die app.js)

**Einstellungen des Projekts** Es handelt sich um ein NPM Projekt. Die **package.json** beinhaltet weitere Informationen/Einstellungen.

## Projekt auf Heroku

**Heroku** https://whispering-woodland-65892.herokuapp.com/

**Fallbeispiel hochladen** example.json als Body über /createbpmn hochladen
