/* 
Hier können diverse entry Points für weitere Javascript Dateien gesetzt werden.
Der Output wird in der index.html bereits geladen & verwendet.
*/
const path = require('path');

module.exports = {
  entry: [
    
    './src/bpmn2.js'
  ],
  output: {
    filename: 'main.js',
    path: path.resolve(__dirname, 'dist')
  },
};

